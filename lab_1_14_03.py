import tkinter as tk
import pandas as pd
import numpy as np
import matplotlib
#import mpl_toolkits
import matplotlib.pyplot as plt
from PIL import Image, ImageTk
from sklearn.cluster import KMeans
from tkinter import Canvas
from mpl_toolkits.mplot3d import Axes3D


## opening
root = tk.Tk()
pilImage = Image.open("ball.jpg")
image = ImageTk.PhotoImage(pilImage)
label = tk.Label(root, image=image)
label.pack()
im = pilImage

## cleaning && preprocessing
res_array = np.array([0, 0, 0])
y = 0
while y < im.height:
    x = 0
    while x < im.width:
        r, g, b = im.getpixel((x,y))
        new_array = np.array([r/255, g/255, b/255])
        res_array = np.vstack((res_array, new_array))
        x += 5
    y += 5
# print(res_array)

## processing
df = pd.DataFrame(res_array, columns = ['r', 'g', 'b'])
K_means = KMeans(init = 'k-means++', n_clusters = 4, n_init = 10).fit(res_array)
labels = K_means.labels_
# print(K_means.labels_)
df['claster'] = K_means.labels_
k = df.groupby('claster').mean()
first_color = np.around(np.array([k['r'][0], k['g'][0], k['b'][0]])*255)
second_color = np.around(np.array([k['r'][1], k['g'][1], k['b'][1]])*255)
third_color = np.around(np.array([k['r'][2], k['g'][2], k['b'][2]])*255)
fofth_color = np.around(np.array([k['r'][3], k['g'][3], k['b'][3]])*255)

## drowing
def dec_to_base(N, base=16):
    if not hasattr(dec_to_base, 'table'):        
        dec_to_base.table = '0123456789ABCDEF'       
    x, y = divmod(N, base)        
    return dec_to_base(x, base) + dec_to_base.table[y] if x else dec_to_base.table[y]
f_c = '#' + str(dec_to_base(int(first_color[0]))) + str(dec_to_base(int(first_color[1]))) + str(dec_to_base(int(first_color[2])))
s_c = '#' + str(dec_to_base(int(second_color[0]))) + str(dec_to_base(int(second_color[1]))) + str(dec_to_base(int(second_color[2])))
t_c = '#' + str(dec_to_base(int(third_color[0]))) + str(dec_to_base(int(third_color[1]))) + str(dec_to_base(int(third_color[2])))
ff_c = '#' + str(dec_to_base(int(fofth_color[0]))) + str(dec_to_base(int(fofth_color[1]))) + str(dec_to_base(int(fofth_color[2])))
size = 600
canvas = Canvas(root, width=size, height=size)
canvas.create_oval(20,20, 120, 120, fill=f_c)
canvas.create_oval(140,20, 240, 120, fill=s_c)
canvas.create_oval(260,20, 360, 120, fill=t_c)
canvas.create_oval(380,20, 480, 120, fill=ff_c)
canvas.pack()
root.update()
#print(res_array)
## x, y, z 
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#print(len(res_array))
i = 0
#print(df['claster'][1])
for i in range(len(res_array) - 1):
    if df['claster'][i] == 0:
        c = f_c
    elif df['claster'][i] == 1:
        c = s_c
    elif df['claster'][i] == 2:
        c = t_c
    else:
        c = ff_c
    ax.scatter(res_array[i][0], res_array[i][1], res_array[i][2], c=c, marker='o')
ax.set_xlabel('RED')
ax.set_ylabel('GREEN')
ax.set_zlabel('BLUE')
plt.show()
root.mainloop()
