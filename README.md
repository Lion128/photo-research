Если вы это читаете, значит, меня, скорее всего, уже нет в живых.
Или люди наконец-то начали читать README перез запуском программ?
# Русский
Если все-таки второе, то:
1) Установите все необходимые библиотеки
при помощи pip install -r requirements.txt
2) После запуска программы появляется окно с фотографией и
основными(центроидными) цветами, которые посчитаны с помощью
метода K-Mean(к-средних). Потом программы немного зависает
(по подсчетам секунд 30 - 40, intel I5 KL) - это нормально.
3) Потом появляется график с раскрашенными точками, координаты которых
соответствуют RGB Данным фотографии.(Те точки рисуются не исходя из их
положения на фотографии, а исходя из их RGB цвета), поэтому картинка
кажется "закрашенной областями". Тем не менее это свидетельствует о
правильно работе алгоритм (который их раскрашивает)

# English (start_up manual)
Long story short what you need to do to set it up:
1) Install everything that's required from file requirements.txt
you can also use command pip install -r requirements.txt
(in the cmd panel)
2) Check if you have got all these files:
ball.jpg
lab_1_14_03.py
requirements.txt
README
3) On win using cmd write: py lab_1_14_03.py
4) After you see a picture you need to wait about 30-40 sec (for Intel I5 KL)
5) You will see both the picture and the 3D figure.
That's all. Thanks for reading.
